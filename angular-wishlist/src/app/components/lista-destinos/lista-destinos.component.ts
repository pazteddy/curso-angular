import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { DestinosApiClient } from '../../models/destinos-api-client.model';
import { AppState } from '../../app.module';
import { Store } from '@ngrx/store';
import { ElegidoFavoritoAction, NuevoDestinoAction } from '../../models/destinos-viajes-state.model';


@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [DestinosApiClient]
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];
  all;

  // destinos: DestinoViaje[];
  constructor( public destinosApiClient: DestinosApiClient, private store: Store<AppState>) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    // Suscribir  al store redux
    this.store.select( state => state.destinos.favorito)
    .subscribe( d => {
      if ( d != null) {
        this.updates.push('Se ha elegido a ' + d.nombre);
      }

    });
    store.select(state =>  state.destinos.items ).subscribe( items => this.all = items);
    // this.destinosApiClient.subscribeOnChange((d: DestinoViaje) => {
    //   if ( d != null) {
    //     this.updates.push('Se ha elegido a ' + d.nombre);
    //   }
    // });
  }

  ngOnInit(): void {
  }

  agregado(d: DestinoViaje) {
    // destinoApiClient
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
    // this.store.dispatch( new NuevoDestinoAction(d));
  }
  elegido(e: DestinoViaje) {
    // this.destinos.forEach( function(x) {x.setSelected(false); });
    // d.setSelected(true);
    this.destinosApiClient.elegir(e);
    // Disparar la accion de redux
    // this.store.dispatch( new ElegidoFavoritoAction(e));
  }

  getAll(){

  }

}
