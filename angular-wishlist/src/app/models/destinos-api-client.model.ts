import { DestinoViaje } from './destino-viaje.model';
import { Subject, BehaviorSubject } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState, db } from 'src/app/app.module';
import { NuevoDestinoAction, ElegidoFavoritoAction } from './destinos-viajes-state.model';
import { Injectable, Inject, forwardRef } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpRequest, HttpClient, HttpResponse } from '@angular/common/http';
import { APP_CONFIG, AppConfig } from '../app.module';

@Injectable()
export class DestinosApiClient {
    // current: Subject<DestinoViaje> =  new BehaviorSubject<DestinoViaje>(null);
    destinos: DestinoViaje[] = [];
    constructor(
      private store: Store<AppState>,
      @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
      private http: HttpClient
  ) {
    // this.destinos = [];
  }
  add(d: DestinoViaje) {
    const headers: HttpHeaders = new HttpHeaders({ 'X-API-TOKEN': 'token-seguridad' });
    const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', { nuevo: d.nombre }, { headers: headers });
    this.http.request(req).subscribe((data: HttpResponse<{}>) => {
        if (data.status === 200) {
            this.store.dispatch(new NuevoDestinoAction(d));
            const myDb = db;
            myDb.destinos.add(d);
            console.log('todos los destinos de la db!');
            myDb.destinos.toArray().then(destinos => console.log(destinos));
        }
    });
}

getById(id: string): DestinoViaje {
    return this.destinos.filter((d) => d.id.toString() === id)[0];
}

getAll(): DestinoViaje[] {
    return this.destinos;

}


elegir(d: DestinoViaje) {
    this.store.dispatch(new ElegidoFavoritoAction(d));
}
}
